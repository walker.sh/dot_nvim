call plug#begin('~/.config/nvim/plugged')

" =========================================================================== 
" I) Plugin Management  
" ===========================================================================

" ---------------------------------------------------------------------------
" Language Agnostic
" ---------------------------------------------------------------------------

" Async maker and linter
Plug 'benekastah/neomake', { 'on': ['Neomake']}
" Async autocomplete
Plug 'Shougo/deoplete.nvim'

" ---------------------------------------------------------------------------
" Golang
" ---------------------------------------------------------------------------

" General Golang plugin
Plug 'fatih/vim-go'

" ---------------------------------------------------------------------------
" JS (ES6, React)
" ---------------------------------------------------------------------------

" General JS support 
Plug 'pangloss/vim-javascript'
" JSX syntax
Plug 'mxw/vim-jsx'
" Typescript syntax
Plug 'leafgarland/typescript-vim'
" JSON syntax
Plug 'sheerun/vim-json'
" Autocomplete support (npm install -g tern) 
Plug 'carlitux/deoplete-ternjs'


" =========================================================================== 
" II) Interface
" =========================================================================== 

" Nerdtree file browser
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeFind', 'NERDTreeToggle'] }
" Lightline (simple status line)
Plug 'bling/vim-airline'
" Buffers tabline
Plug 'ap/vim-buftabline'

" ---------------------------------------------------------------------------
" Colorschemes 
" ---------------------------------------------------------------------------
Plug 'altercation/vim-colors-solarized'

" ---------------------------------------------------------------------------
" Writing 
" ---------------------------------------------------------------------------
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'danielbmarques/vim-ditto'
Plug 'reedes/vim-wordy'

call plug#end()

" ---------------------------------------------------------------------------
" Basic settings 
" ---------------------------------------------------------------------------

set shell=/bin/bash                     " Set shell to zsh
set number                             " Line numbers on
set showmode                           " Always show mode
set showcmd                            " Show commands as you type them
set textwidth=120                      " Text width is 120 characters
set cmdheight=1                        " Command line height
set clipboard+=unnamed                 " Allow use of system keyboard
set lazyredraw                         " Don't redraw during macro execution
set synmaxcol=160                      " Don't highlight minified files

" ---------------------------------------------------------------------------
" Tab settings
" ---------------------------------------------------------------------------
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" ---------------------------------------------------------------------------
" Split settings 
" ---------------------------------------------------------------------------

set splitbelow                         " :sp sets new window below
set splitright                         " :vsp sets new window to right

" ---------------------------------------------------------------------------
" Spelling settings 
" ---------------------------------------------------------------------------
set spellfile=~/.config/nvim/dictionary.utf-8.add
set spelllang=en_us                    " Set language to US English
set nospell                            " Disable spellcheck by default (<F4> to toggle)

" ---------------------------------------------------------------------------
" Colorscheme settings 
" ---------------------------------------------------------------------------
syntax enable
set t_Co=256
set background=dark
colorscheme solarized

" ---------------------------------------------------------------------------
" Key rebindings  
" ---------------------------------------------------------------------------

" Change ESC to jk 
imap jk <Esc>
imap kj <Esc>

" Swap colon and semicolon
nnoremap : ;
nnoremap ; :

map <C-n> :NERDTreeToggle

" ---------------------------------------------------------------------------
"  Leader settings
" ---------------------------------------------------------------------------
" Set Leader to space
let mapleader = "\<Space>"

" Leader shortcuts
nnoremap <Leader>w :w<CR>  " Save file with <Space>w
nnoremap <Leader>q :wq<CR> " Save/quit with <Space>q

" Autocommands

autocmd vimenter * :NERDTreeToggle



" ---------------------------------------------------------------------------
"  Plugin configurations
" ---------------------------------------------------------------------------
" Limelight
let g:limelight_conceal_ctermfg = 'gray'
